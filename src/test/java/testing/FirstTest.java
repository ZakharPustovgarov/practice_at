package testing;

import ForTest.HomePage;
import ForTest.GamePage;
import ForTest.GamesPage;
import ForTest.SearchPage;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import io.qameta.allure.junit4.DisplayName;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;


import static org.junit.Assert.assertEquals;


public class FirstTest {

    private static WebDriver driver;

    private static WebDriverWait waitFor;

    private static JavascriptExecutor jse;


    @Before
    public void RunDriver(){
        OperaOptions options = new OperaOptions();
        options.setBinary("C:\\Users\\deads\\AppData\\Local\\Programs\\Opera\\60.0.3255.151\\opera.exe");
        System.setProperty("webdriver.opera.driver", "C:\\Users\\deads\\AppData\\Local\\Programs\\Opera\\operadriver_win64\\operadriver.exe");
        driver = new OperaDriver(options);
        waitFor = new WebDriverWait(driver, 10, 1000);
        jse = (JavascriptExecutor) driver;
    }

    @Attachment (value = "{name}", type = "image/png")
    public static byte[] getFullScreenshot(WebDriver driver, String name) throws IOException {
        WebDriver augmented = new Augmenter().augment(driver);
        Screenshot shot = new AShot().takeScreenshot(augmented);
        BufferedImage img = shot.getImage();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(img, "png", baos);
        baos.flush();
        byte[] imageInByte = baos.toByteArray();
        baos.close();
        return imageInByte;
    }
    @Attachment (value = "{name}", type = "image/png")
    public static byte[] getElementScreenshot(WebDriver driver, String name, WebElement element) throws IOException {
        WebDriver augmented = new Augmenter().augment(driver);
        Screenshot shot = new AShot()
                .coordsProvider(new WebDriverCoordsProvider())
                .takeScreenshot(augmented, element);
        BufferedImage img = shot.getImage();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(img, "png", baos);
        baos.flush();
        byte[] imageInByte = baos.toByteArray();
        baos.close();
        return imageInByte;
    }
    @Rule
    public TestWatcher screenshotOnFailure = new TestWatcher() {
        @Override
        protected void failed(Throwable e, Description description) {
            try {
                getFullScreenshot(driver,"Скриншот при ошибке");
            } catch (IOException f)
            {
            }
        }
    };
    @Step("Заходим на сайт Steam")
    public static void IntoSteam( ) throws IOException
    {
        driver.get("https://store.steampowered.com");
        assertEquals("Добро пожаловать в Steam",driver.getTitle());
        getFullScreenshot(driver, "Главная страница Steam");
    }
    @Step("Нажатие в меню на кнопку 'Игры'")
    public static void ClickGames( ) throws IOException
    {
        HomePage hp = new HomePage(driver);
        waitFor.until(ExpectedConditions.visibilityOfElementLocated(
                By.linkText("Игры")));
        hp.GenreClick();
        getFullScreenshot(driver, "переход на страницу 'Игры'");
    }
    @Step("Поиск товара 'Destiny 2 Shadowkeep'")
    public static void Search ( ) throws IOException
    {
        GamesPage gsp = new GamesPage(driver);
        gsp.SearchFor("Destiny 2 Shadowkeep");
        getFullScreenshot(driver, "Поиск игры по названию");
    }

    @Step("Нажатие на товар из списка")
    public static void SelectItem( ) throws IOException
    {
        assertEquals("Поиск Steam", driver.getTitle());
        SearchPage sp = new SearchPage(driver);
        sp.clickItem();
        getFullScreenshot(driver, "Переход на первый результат поиска");
    }
    @Step("Нажатие на кнопку 'Поделиться', затем 'ОК'")
    public static void Share( ) throws IOException,InterruptedException
    {
        jse.executeScript("scroll(0,1500)");
        GamePage gp = new GamePage(driver);
        gp.ShareClick();
        assertEquals("ПОДЕЛИТЬСЯ", driver.findElement(By.xpath("//div[@class='newmodal']//div[@class='title_text']")).getText() );
        getFullScreenshot(driver, "Модальное окно 'Поделиться'");
        gp.SharOkClick();
        Thread.sleep(300);
    }

    @Step("Нажатие на кнопку 'HTML-код', выбор поизиции, закрытыие окна")
    public static void Widget( ) throws IOException
    {
        GamePage gp = new GamePage(driver);
        gp.widgetClick();
        getFullScreenshot(driver, "Модальное окно 'СОЗДАТЬ ВИДЖЕТ ДЛЯ ВСТАВКИ НА САЙТ'");
        gp.widgetCloseClick();
    }

    @Step("Выход из браузера")
    public static void WaitAndQuit() throws InterruptedException
    {
        Thread.sleep(1000);
        driver.quit();
    }

    @Test
    @DisplayName("Тест для проверки поиска и меню")
    public void Test1() throws InterruptedException,IOException
    {
        IntoSteam();
        ClickGames();
        Search();
        SelectItem();
        Share();
        Widget();
        WaitAndQuit();
    }
}