package testing;

import ForTest.GamePage;
import ForTest.HomePage;
import ForTest.SearchPage;
import ForTest.DeveloperPage;
import ForTest.SubPage;
import ForTest.AgeCheckPage;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import io.qameta.allure.junit4.DisplayName;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.*;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static testing.FirstTest.getFullScreenshot;

public class SecondTest {

    private static WebDriver driver;

    private static WebDriverWait waitFor;

    private static JavascriptExecutor jse;


    @Before
    public void RunDriver(){
        OperaOptions options = new OperaOptions();
        options.setBinary("C:\\Users\\deads\\AppData\\Local\\Programs\\Opera\\60.0.3255.151\\opera.exe");
        System.setProperty("webdriver.opera.driver", "C:\\Users\\deads\\AppData\\Local\\Programs\\Opera\\operadriver_win64\\operadriver.exe");
        driver = new OperaDriver(options);
        waitFor = new WebDriverWait(driver, 10, 1000);
        jse = (JavascriptExecutor) driver;
    }



    @Rule
    public TestWatcher screenshotOnFailure = new TestWatcher() {
        @Override
        protected void failed(Throwable e, Description description) {
            try {
                getFullScreenshot(driver,"Скриншот при ошибке");
            } catch (IOException f)
            {
            }
        }
    };

    @Step("Заходим на сайт Steam")
    public static void IntoSteam() throws IOException
    {
        driver.get("https://store.steampowered.com");
        assertEquals("Добро пожаловать в Steam",driver.getTitle());
        getFullScreenshot(driver, "Главная страница Steam");
    }
    @Step("Прокрутка карусели на домашней странице и клик по четвёртому элементу")
    public static void carouselSpin(WebDriver driver) throws InterruptedException
    {
        HomePage hp = new HomePage(driver);
        hp.CarouselScrollRightFor(5);
        hp.CarouselScrollLeftFor(2);
        hp.CarouselItemClick();
    }
    @Step("Проверка возраста")
    public static void AgeCheck() throws InterruptedException
    {
        Thread.sleep(1000);
        if(driver.getCurrentUrl().contains("https://store.steampowered.com/agecheck"))
        {
            AgeCheckPage agp = new AgeCheckPage(driver);
            agp.AgeVerify();
        }
    }
    @Step("Проверка на комплект")
    public static void SubCheck() throws InterruptedException
    {
        Thread.sleep(1000);
        if(driver.getCurrentUrl().contains("https://store.steampowered.com/sub"))
        {
            SubPage sp = new SubPage(driver);
            sp.ItemChoose();
        }
    }
    @Step("Клик по рекомендованной на распродаже игре")
    public static void saleRecomendClick() throws IOException
    {
        HomePage hp = new HomePage(driver);
        hp.SaleRecomItemClick();
        getFullScreenshot(driver, "Переход к рекомендованному товару на главной странице");
    }
    @Step("Переход на страницу поиска игр с одинаковым свойстовм")
    public static void tagClick() throws IOException
    {
        waitFor.until(ExpectedConditions.visibilityOfElementLocated(
                By.className("apphub_AppName")));
        assertTrue(driver.getCurrentUrl().contains("https://store.steampowered.com/app"));
        GamePage gp = new GamePage(driver);
        jse.executeScript("scroll(0,900)");
        gp.detailClick();
        getFullScreenshot(driver, "Переход к играм со схожим свойством");
    }
    @Step("Ставиться фильтр и переход на первую игру в списке")
    public static void filterClickAndOnGP() throws IOException,InterruptedException
    {
        assertTrue(driver.getCurrentUrl().contains("https://store.steampowered.com/search"));
        SearchPage sp = new SearchPage(driver);
        sp.filterClick();
        Thread.sleep(1000);
        sp.clickItem();
        getFullScreenshot(driver, "Добавление фильтра в поиске и переход к первой игре из списка");
    }
    @Step("Переход на страницу разработчиков игры")
    public static void developerClick() throws IOException
    {
        assertTrue(driver.getCurrentUrl().contains("https://store.steampowered.com/app"));
        GamePage gp = new GamePage(driver);
        gp.developerClick();
        getFullScreenshot(driver, "Переход к странице разработчика");
    }

    @Step("Переход к рекомендованным играм разработчика")
    public static void developerRecom() throws IOException
    {
        DeveloperPage dp = new DeveloperPage(driver);
        dp.NavRecomClick();
        dp.RecomItemClick();
        getFullScreenshot(driver, "Переход к рекомендованной игре от разработчика");
    }
    @Step("Выход из браузера")
    public static void WaitAndQuit() throws InterruptedException,IOException
    {
        Thread.sleep(1000);
        driver.quit();
    }

    @Test
    @DisplayName("Тест для проверки тэгов и страниц разработчиков")
    public void Test2() throws InterruptedException,IOException
    {
        IntoSteam();
        //carouselSpin(driver);
        //AgeCheck(driver,waitFor);
        //SubCheck(driver,waitFor);
        saleRecomendClick();
        tagClick();
        filterClickAndOnGP();
        developerClick();
        developerRecom();
        WaitAndQuit();
    }
}
