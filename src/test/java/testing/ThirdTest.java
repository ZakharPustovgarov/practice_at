package testing;

import ForTest.*;
import io.qameta.allure.Step;
import io.qameta.allure.junit4.DisplayName;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;

import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static testing.FirstTest.getFullScreenshot;

public class ThirdTest {

    private static WebDriver driver;

    private static WebDriverWait waitFor;

    private static JavascriptExecutor jse;


    @Before
    public void RunDriver(){
        OperaOptions options = new OperaOptions();
        options.setBinary("C:\\Users\\deads\\AppData\\Local\\Programs\\Opera\\60.0.3255.151\\opera.exe");
        System.setProperty("webdriver.opera.driver", "C:\\Users\\deads\\AppData\\Local\\Programs\\Opera\\operadriver_win64\\operadriver.exe");
        driver = new OperaDriver(options);
        waitFor = new WebDriverWait(driver, 10, 1000);
        jse = (JavascriptExecutor) driver;
    }

    @Rule
    public TestWatcher screenshotOnFailure = new TestWatcher() {
        @Override
        protected void failed(Throwable e, Description description) {
            try {
                getFullScreenshot(driver,"Скриншот при ошибке");
            } catch (IOException f)
            {
            }
        }
    };

    @Step("Заходим на сайт Steam")
    public static void IntoSteam( ) throws IOException
    {
        driver.get("https://store.steampowered.com");
        assertEquals("Добро пожаловать в Steam",driver.getTitle());
        getFullScreenshot(driver,"Главная страница Steam");
    }
    @Step("Переход на страницу логина")
    public static void IntoLogIn( ) throws IOException
    {
        HomePage hp = new HomePage(driver);
        hp.ToLogIn();
        getFullScreenshot(driver, "Переход на страницу входа в учётную запись");
    }
    @Step("Ввод неправильных данных")
    public static void InvalidUser( ) throws IOException
    {
        LoginPage lp = new LoginPage(driver);
        lp.InputInvalidUser();
        getFullScreenshot(driver, "Ввод неправильных данных");
    }
    @Step("Проверка ошибки ввода")
    public static void CheckForError( ) throws IOException
    {
        LoginPage lp = new LoginPage(driver);
        lp.CheckError();
        getFullScreenshot(driver, "Видимая ошибка ввода данных");
    }
    @Step("Ввод правильных данных")
    public static void ValidUser( ) throws IOException,InterruptedException
    {
        Thread.sleep(7000);
        LoginPage lp = new LoginPage(driver);
        lp.InputValidUser();
        getFullScreenshot(driver, "Ввод правильных данных");
    }
    @Step("Поиск товара 'Cyberpunk 2077'")
    public static void Search ( ) throws IOException
    {
        HomePage hp = new HomePage(driver);
        hp.WaitUser();
        hp.SearchFor("Cyberpunk 2077");
        getFullScreenshot(driver, "Поиск игры по названию");
    }
    @Step("Нажатие на товар из списка")
    public static void SelectItem( ) throws IOException
    {
        assertEquals("Поиск Steam", driver.getTitle());
        SearchPage sp = new SearchPage(driver);
        sp.clickItem();
        getFullScreenshot(driver, "Переход к игре из списка поиска");
    }
    @Step("Добавление Cyberpunk 2077 в список желаемого")
    public static void AddToWish( ) throws IOException
    {
        GamePage gp = new GamePage(driver);
        gp.AddWishClick();
        getFullScreenshot(driver, "Добавление игры в список желаемого");
        gp.ToStrategyGenre();
    }
    @Step("Переход на страницу Экшн игры")
    public static void GoToStrategyGame( ) throws IOException
    {
        GamesPage gsp = new GamesPage(driver);
        getFullScreenshot(driver, "Страница игр с жанром 'Стартегии'");
        gsp.GoToGame();
    }
    @Step("Добавление игры в корзину")
    public static void AddToCart( ) throws IOException
    {
        GamePage gp = new GamePage(driver);
        gp.AddCartClick();
        getFullScreenshot(driver, "Добавление игры в корзину");
    }
    @Step("Переход к списку желаемого")
    public static void FromCartToWish( ) throws  IOException
    {
        CartPage cp = new CartPage(driver);
        getFullScreenshot(driver,"Корзина до добавления из списка желамеого");
        cp.ToWish();
    }
    @Step("Добавление игры из списка желаемого в корзину")
    public static void GameFromWishToCart( ) throws IOException
    {
        WishPage wp = new WishPage(driver);
        getFullScreenshot(driver,"Список желаемого");
        wp.WishToCart();
    }
    @Step("Удаление предметов из корзины")
    public static void CartRemoveAll( ) throws IOException
    {
        CartPage cp = new CartPage(driver);
        getFullScreenshot(driver,"Корзина после добавления из списка желамеого");
        cp.RemoveAll();
        cp.RemoveOk();
        getFullScreenshot(driver, "Корзина полсе удаления товаров из неё");
    }
    @Step("Удаление игры из списка желаемого")
    public static void DeleteWish( ) throws IOException
    {
        WishPage wp = new WishPage(driver);
        wp.RemoveWish();
        wp.RemoveOk();
        getFullScreenshot(driver, "Список желаемого после удаления игры из списка");
    }
    @Step("Выход из браузера")
    public static void WaitAndQuit( ) throws InterruptedException
    {
        Thread.sleep(1000);
        driver.quit();
    }

    @Test
    @DisplayName("Тест для проверки входа, списка желаемого и корзины")
    public void Test3() throws InterruptedException,IOException
    {
        IntoSteam();
        IntoLogIn();
        InvalidUser();
        CheckForError();
        ValidUser();
        Search();
        SelectItem();
        AddToWish();
        GoToStrategyGame();
        AddToCart();
        FromCartToWish();
        GameFromWishToCart();
        CartRemoveAll();
        FromCartToWish();
        DeleteWish();
        WaitAndQuit();
    }
}
