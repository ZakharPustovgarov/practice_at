package ForTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
public class SubPage {

    private WebDriver driver;

    private static String STEAM_URL = "https://store.steampowered.com";

    WebDriverWait waitFor;

    @FindBy(xpath = "//div[@class='leftcol game_description_column'][1]/div[@class='tab_item ']/a[1]")
    private WebElement bFirstItem;


    public SubPage(WebDriver driver) {
        if (!driver.getCurrentUrl().contains(STEAM_URL)) {
            throw new IllegalStateException("This is not the page you are expected");
        }
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.waitFor = new WebDriverWait(driver, 10, 1000);
    }

    public void ItemChoose(){
        waitFor.until(ExpectedConditions.visibilityOf(bFirstItem));
        bFirstItem.click();
    }
}
