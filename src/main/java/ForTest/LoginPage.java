package ForTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {

    private WebDriver driver;

    private static String STEAM_URL = "https://store.steampowered.com";

    WebDriverWait waitFor;

    private static class UserValid
    {
        static String  Name = "silvertitan1999";
        static String Pass = "Silverteton";
    }

    private static class UserInvalid
    {
        static String Name = "silvertitan1999";
        static String Pass = "silvertaton";
    }

    @FindBy(id = "input_username")
    private WebElement login_name;

    @FindBy(id = "input_password")
    private WebElement login_pass;

    @FindBy(xpath = "//div[@id='login_btn_signin']/button[@type='submit']")
    private WebElement bConfirm;

    @FindBy(className = "checkout_error")
    private WebElement ErrorBox;

    public LoginPage(WebDriver driver) {
        if (!driver.getCurrentUrl().contains(STEAM_URL)) {
            throw new IllegalStateException("This is not the page you are expected");
        }
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.waitFor = new WebDriverWait(driver, 10, 1000);
    }

    public void InputInvalidUser(){
        login_name.sendKeys(UserInvalid.Name);
        login_pass.sendKeys(UserInvalid.Pass);
        bConfirm.click();
    }

    public void InputValidUser(){
        login_name.clear();
        login_pass.clear();
        login_name.sendKeys(UserValid.Name);
        login_pass.sendKeys(UserValid.Pass);
        bConfirm.click();

    }

    public void CheckError()
    {
        waitFor.until(ExpectedConditions.visibilityOf(ErrorBox));
    }

}
