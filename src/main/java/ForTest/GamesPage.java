package ForTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GamesPage {

    private WebDriver driver;

    private static String STEAM_URL = "https://store.steampowered.com";

    WebDriverWait waitFor;

    @FindBy(id = "store_nav_search_term")
    private WebElement searchBox;

    @FindBy(xpath = "//div[@id='page_section_container']//div[@class='carousel_items  store_capsule_container']/div[@class='focus']/a[1]")
    private WebElement onGamepage;

    public GamesPage(WebDriver driver) {
        if (!driver.getCurrentUrl().contains(STEAM_URL)) {
            throw new IllegalStateException("This is not the page you are expected");
        }
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.waitFor = new WebDriverWait(driver, 10, 1000);
    }

    public void SearchFor(String item){
        waitFor.until(ExpectedConditions.visibilityOf(searchBox));
        searchBox.sendKeys(item);
        searchBox.submit();
    }

    public void GoToGame() {
        waitFor.until(ExpectedConditions.visibilityOf(onGamepage));
        onGamepage.click();
    }
}
