package ForTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {

    private WebDriver driver;

    private static String STEAM_URL = "https://store.steampowered.com";

    WebDriverWait waitFor;

    @FindBy(id = "store_nav_search_term")
    private WebElement searchBox;

    @FindBy(className = "carousel_items")
    private WebElement CarousItem;

    @FindBy(className = "right")
    private WebElement CarousRightAr;

    @FindBy(className = "left")
    private WebElement CarousLeftAr;

    @FindBy(xpath = "//div[@class='home_featured_ctn home_pagecontent_ctn']/div[@id='featured_hero_items']/div[2]/a")
    private WebElement SaleRecomItem;

    @FindBy(id = "genre_tab")
    private WebElement bGenre;

    @FindBy(xpath = "//div[@id='global_action_menu']/a[@class='global_action_link']")
    private WebElement bEnterLogin;

    @FindBy(id = "account_pulldown")
    private WebElement bAccountName;

    @FindBy(xpath = "//div[@id='account_dropdown']/div[@class='popup_body popup_menu']/a[2]")
    private WebElement bAcc;

    public HomePage(WebDriver driver) {
        if (!driver.getCurrentUrl().contains(STEAM_URL)) {
            throw new IllegalStateException("This is not the page you are expected");
        }
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.waitFor = new WebDriverWait(driver, 10, 1000);
    }

    public void SearchFor(String item){
        waitFor.until(ExpectedConditions.visibilityOf(searchBox));
        searchBox.sendKeys(item);
        searchBox.submit();
    }

    public void WaitUser(){
        waitFor.until(ExpectedConditions.visibilityOf(bAccountName));
    }

    public void GenreClick(){
        bGenre.click();
    }

    public void CarouselScrollRightFor(int count) throws InterruptedException{
        for (int i = 0; i < count;i++)
        {
            CarousRightAr.click();
            Thread.sleep(300);
        }
    }

    public void CarouselScrollLeftFor(int count) throws InterruptedException{
        for (int i = 0; i < count;i++)
        {
            CarousLeftAr.click();
            Thread.sleep(300);
        }
    }

    public void SaleRecomItemClick(){
        SaleRecomItem.click();
    }

    public void CarouselItemClick(){
            CarousItem.click();
    }

    public void ToLogIn() {
        bEnterLogin.click();
    }
}
