package ForTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AgeCheckPage {


    private WebDriver driver;

    private static String STEAM_URL = "https://store.steampowered.com";

    WebDriverWait waitFor;

    @FindBy(id = "ageYear")
    private WebElement sAgeYear;

    @FindBy(xpath = "//select[@id='ageYear']/option[@value='2000']")
    private WebElement opAge2000;

    @FindBy(xpath = "//a[@onclick='ViewProductPage()']/span")
    private WebElement bGoToGame;

    public AgeCheckPage(WebDriver driver) {
        if (!driver.getCurrentUrl().contains(STEAM_URL)) {
            throw new IllegalStateException("This is not the page you are expected");
        }
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.waitFor = new WebDriverWait(driver, 10, 1000);
    }

    public void AgeVerify(){
        waitFor.until(ExpectedConditions.visibilityOf(sAgeYear));
        sAgeYear.click();
        opAge2000.click();
        bGoToGame.click();
    }

}
