package ForTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CartPage {

    private WebDriver driver;

    private static String STEAM_URL = "https://store.steampowered.com";

    private WebDriverWait waitFor;

    @FindBy(id = "wishlist_link")
    private WebElement bToWish;

    @FindBy(xpath = "//div[@class='checkout_notes']//a[@class='remove_link']")
    private WebElement bRemoveAll;

    @FindBy(xpath = "//div[@class='newmodal']//div[@class='newmodal_buttons']/div[@class='btn_green_white_innerfade btn_medium']/span")
    private WebElement bRemoveOk;

    public CartPage(WebDriver driver) {
        if (!driver.getCurrentUrl().contains(STEAM_URL)) {
            throw new IllegalStateException("This is not the page you are expected");
        }
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.waitFor = new WebDriverWait(driver, 10, 1000);
    }

    public void ToWish() {
        waitFor.until(ExpectedConditions.visibilityOf(bToWish));
        bToWish.click();
    }

    public void RemoveAll() {
        waitFor.until(ExpectedConditions.visibilityOf(bRemoveAll));
        bRemoveAll.click();
    }

    public void RemoveOk(){
        waitFor.until(ExpectedConditions.visibilityOf(bRemoveOk));
        bRemoveOk.click();
    }
}
