package ForTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
public class DeveloperPage {

    private WebDriver driver;

    private static String STEAM_URL = "https://store.steampowered.com";

    WebDriverWait waitFor;

    @FindBy(xpath = "//div[@class='navigation_bar']/a[@class=' option_browse']")
    private WebElement bRecom;

    @FindBy(xpath = "//div[@id='RecommendationsTable']//div[@class='recommendation'][1]")
    private WebElement bRecomItem;

    public DeveloperPage(WebDriver driver) {
        if (!driver.getCurrentUrl().contains(STEAM_URL)) {
            throw new IllegalStateException("This is not the page you are expected");
        }
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.waitFor = new WebDriverWait(driver, 10, 1000);
    }

    public void NavRecomClick(){
        waitFor.until(ExpectedConditions.visibilityOf(bRecom));
        bRecom.click();
    }

    public void RecomItemClick(){
        waitFor.until(ExpectedConditions.visibilityOf(bRecomItem));
        bRecomItem.click();
    }
}
