package ForTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
public class WishPage {

    private WebDriver driver;

    private static String STEAM_URL = "https://store.steampowered.com";

    private WebDriverWait waitFor;

    @FindBy(xpath = "//div[@class='addedon']/div[@class='delete']")
    private WebElement bRemoveWish;

    @FindBy(xpath = "//form[@name='add_to_cart_367653']//a[@class='btnv6_green_white_innerfade btn_medium noicon']")
    private WebElement bWishToCart;

    @FindBy(xpath = "//div[@class='newmodal']//div[@class='newmodal_buttons']/div[@class='btn_green_white_innerfade btn_medium']/span")
    private WebElement bRemoveOk;

    public WishPage(WebDriver driver) {
        if (!driver.getCurrentUrl().contains(STEAM_URL)) {
            throw new IllegalStateException("This is not the page you are expected");
        }
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.waitFor = new WebDriverWait(driver, 10, 1000);
    }

    public void WishToCart() {
        waitFor.until(ExpectedConditions.visibilityOf(bWishToCart));
        bWishToCart.click();
    }

    public void RemoveWish() {
        waitFor.until(ExpectedConditions.visibilityOf(bRemoveWish));
        bRemoveWish.click();
    }

    public void RemoveOk(){
        waitFor.until(ExpectedConditions.visibilityOf(bRemoveOk));
        bRemoveOk.click();
    }
}
