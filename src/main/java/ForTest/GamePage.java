package ForTest;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GamePage {

    private WebDriver driver;

    private static String STEAM_URL = "https://store.steampowered.com";

    private WebDriverWait waitFor;

    @FindBy(linkText = "Поделиться")
    private WebElement Share;

    @FindBy(xpath = "//div [@class='dev_row']/div[@id='developers_list']/a[1]")
    private WebElement developer;

    @FindBy(xpath = "//div[@id='category_block']/div[1]//img[@class='category_icon']")
    private WebElement detail;

    @FindBy(xpath = "//a[@onclick='ShowEmbedWidget(1085660); return false;']/span")
    private WebElement widget;

    @FindBy(xpath = "//a[@onclick='CreateWidget(1085660); return false;']/span")
    private WebElement widgetCreate;

    @FindBy(xpath = "//input[@id='wp_369781']")
    private WebElement widgetRadio;

    @FindBy(xpath = "//div[@class='newmodal_close']")
    private WebElement widgetClose;

    @FindBy(className = "btn_grey_white_innerfade")
    private WebElement bOk;

    @FindBy(xpath = "//div[@id='add_to_wishlist_area']/a[@class='btnv6_blue_hoverfade btn_medium']")
    private WebElement bAddWish;

    @FindBy(xpath = "//div[@class='btn_addtocart']/a[@class='btnv6_green_white_innerfade btn_medium']")
    private WebElement bAddCart;

    @FindBy(xpath = "//div[@id='genre_tab']/span[@class='pulldown']/span")
    private WebElement bSpanGenre;

    @FindBy(xpath = "//div[@id='genre_flyout']/div[@class='popup_body popup_menu']/a[@class='popup_menu_item'][15]")
    private WebElement bGenreAction;

    public GamePage(WebDriver driver) {
        if (!driver.getCurrentUrl().contains(STEAM_URL)) {
            throw new IllegalStateException("This is not the page you are expected");
        }
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.waitFor = new WebDriverWait(driver, 10, 1000);
    }

    public void ShareClick ()
    {
        waitFor.until(ExpectedConditions.visibilityOf(Share));
        Share.click();
    }

    public void widgetClick ()
    {
        waitFor.until(ExpectedConditions.visibilityOf(widget));
        widget.click();
        widgetRadio.click();
        widgetCreate.click();
    }

    public void widgetCloseClick ()
    {
        waitFor.until(ExpectedConditions.visibilityOf(widgetClose));
        widgetClose.click();
    }

    public void SharOkClick ()
    {
        bOk.click();
    }

    public void developerClick ()
    {
        developer.click();
    }

    public void detailClick ()
    {
        waitFor.until(ExpectedConditions.visibilityOf(detail));
        detail.click();
    }

    public void AddWishClick ()
    {
        waitFor.until(ExpectedConditions.visibilityOf(bAddWish));
        bAddWish.click();
        waitFor.until(ExpectedConditions.visibilityOfElementLocated(By.className("wishlist_added_temp_notice")));
    }
    public void AddCartClick ()
    {
        waitFor.until(ExpectedConditions.visibilityOf(bAddCart));
        bAddCart.click();
    }

    public void ToStrategyGenre(){
        waitFor.until(ExpectedConditions.visibilityOf(bSpanGenre));
        bSpanGenre.click();
        bGenreAction.click();
    }
}
