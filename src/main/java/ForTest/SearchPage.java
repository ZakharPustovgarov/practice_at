package ForTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchPage {

    private WebDriver driver;

    private static String STEAM_URL = "https://store.steampowered.com";

    private WebDriverWait waitFor;

    @FindBy(xpath = "//div[@id='search_result_container']/div[2]/a[1]")
    private WebElement searchItem;

    @FindBy(xpath = "//div[@data-loc='Игры']")
    private WebElement gamesFilter;

    public SearchPage(WebDriver driver) {
        if (!driver.getCurrentUrl().contains(STEAM_URL)) {
            throw new IllegalStateException("This is not the page you are expected");
        }
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.waitFor = new WebDriverWait(driver, 10, 1000);
    }

    public void clickItem(){
        waitFor.until(ExpectedConditions.visibilityOf(searchItem));
        searchItem.click();
    }

    public void filterClick(){
        gamesFilter.click();
    }

}
